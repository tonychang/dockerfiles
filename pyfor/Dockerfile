FROM ubuntu:16.04 

ENV CPLUS_INCLUDE_PATH=/usr/include/gdal
ENV C_INCLUDE_PATH=/usr/include/gdal

#build the LASzip and LAStools for decompressing .laz to .las
RUN apt-get update && apt-get install -y git build-essential cmake wget unzip
RUN git clone https://github.com/LASzip/LASzip.git && cd LASzip && git checkout 3.1.0
WORKDIR LASzip
RUN cmake . && make && make install
WORKDIR /
RUN wget --quiet http://www.cs.unc.edu/~isenburg/lastools/download/LAStools.zip
RUN unzip LAStools.zip && cd LAStools && make
RUN cp /LAStools/bin/laszip /usr/local/bin
RUN ln -s /usr/local/bin/laszip /usr/local/bin/laszip-cli 

#build python3.6 
RUN apt-get update 
RUN apt-get install -y software-properties-common 
RUN add-apt-repository ppa:deadsnakes/ppa
RUN add-apt-repository ppa:jonathonf/python-3.6
RUN apt-get update 
RUN apt-get install -y python3.6-dev python3.6-venv wget git
RUN wget https://bootstrap.pypa.io/get-pip.py
RUN python3.6 get-pip.py
RUN ln -s /usr/bin/python3.6 /usr/local/bin/python3
RUN pip install --upgrade pip


#install gdal dependencies
RUN apt-get install -y libgdal-dev \
    gdal-bin \
    python3-gdal \
    build-essential \
    && apt-get autoremove \
    && rm -rf /var/lib/apt /var/cache/apt 

#pip install dependencies for pyfor
RUN pip install \
    numpy \
    pandas \
    laspy \
    plyfile \
    matplotlib \
    rasterio \
    shapely \
    geopandas \
    plotly \
    numba \
    laxpy \
    scipy \
    scikit-image \
    descartes \
    jupyter

#pyfor install
RUN pip install git+https://github.com/brycefrank/pyfor.git@0.3.2 

#jupyter extensions
RUN mkdir -p $(jupyter --data-dir)/nbextensions 
RUN cd $(jupyter --data-dir)/nbextensions && \
    git clone https://github.com/lambdalisue/jupyter-vim-binding vim_binding && \
    jupyter nbextension enable vim_binding/vim_binding

